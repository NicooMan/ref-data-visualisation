"use strict";

function stackedbarchart(targetDOMelement) {
  var stackedBarchartObject = {};

  stackedBarchartObject.loadAndRenderDataset = function (data) {
    dataset = data;
    render();
    return stackedBarchartObject;
  };

  var mouseOverFunction = function (d) {
    d3.selectAll(".dots#" + d.data.university.replace(/\s/g, "-"))
      .classed("selected", true)
      .attr("r", 8);

    var stringSelect =
      ".dots#" +
      d.data.university.replace(/\s/g, "-") +
      "[uoa='" +
      d.data.UoA +
      "']";
    d3.selectAll(stringSelect).classed("highlight", true).attr("r", 10);
  };

  var mouseOutFunction = function (d) {
    d3.selectAll(".dots").classed("highlight", false);
    d3.selectAll(".dots").classed("selected", false);
    d3.selectAll(".dots").attr("r", 6);
  };

  var dataset = [];
  var margin = { top: 60, bottom: 50, left: 50, right: 20 };
  var width = 1000 - margin.left - margin.right;
  var height = 400 - margin.top - margin.bottom;
  var yIndent = 150;

  var keys = ["environment4ratings", "impact4ratings", "outputs4ratings"];

  var colors = d3.scaleOrdinal(d3.schemeCategory20);

  var svg = d3
    .select(targetDOMelement)
    .append("svg")
    .attr("width", width + margin.top + margin.bottom)
    .attr("height", height + margin.left + margin.right)
    .classed("stackedbarchart", true);

  var g = svg
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var xAxis = g.append("g").attr("class", "xAxis");

  var yAxis = g
    .append("g")
    .attr("class", "yAxis")
    .attr("transform", "translate(" + yIndent + ",0)");

  var legend = g
    .append("g")
    .attr("class", "legend")
    .selectAll("g")
    .data(keys.slice().reverse())
    .enter()
    .append("g")
    .attr("transform", function (d, i) {
      return "translate(0," + i * 25 + ")";
    });

  legend
    .append("rect")
    .attr("x", width - 180)
    .attr("width", 20)
    .attr("height", 20)
    .attr("fill", colors)
    .attr("opacity", 0.7);

  legend
    .append("text")
    .attr("x", width - 155)
    .attr("y", 12.5)
    .text(function (d) {
      var trans = {
        environment4ratings: "Environment",
        impact4ratings: "Impact",
        outputs4ratings: "Outputs",
      };
      return trans[d];
    });

  g.append("text")
    .attr("class", "stackedbarchart--label")
    .classed("xlabel", true)
    .attr("transform", "translate(" + width / 2 + ",-25)")
    .attr("text-anchor", "middle")
    .text("Cumulative 4* ratings %");

  function render() {
    var xScale = d3.scaleLinear().range([yIndent, width - 200]);
    xScale.domain([0, d3.max(dataset, (d) => d3.sum(keys, (k) => +d[k]))]);

    var yScale = d3.scaleBand().rangeRound([0, height]).padding(0.2);
    yScale.domain(dataset.map((d) => d.university));

    xAxis.transition(1000).call(d3.axisTop(xScale));
    yAxis.transition(1000).call(d3.axisLeft(yScale));

    var stack = d3.stack().keys(keys);
    var stackedData = stack(dataset);

    var layersGroup = g.selectAll("g.layer").data(stackedData);

    layersGroup
      .enter()
      .append("g")
      .classed("layer", true)
      .attr("fill", (d) => colors(d.key))
      .attr("opacity", 0.7);

    var bars = g
      .selectAll("g.layer")
      .selectAll("rect")
      .data(
        (d) => d,
        (e) => e.data.university
      );

    var enterBars = bars.enter().append("rect");

    enterBars
      .merge(bars)
      .attr("height", yScale.bandwidth())
      .transition()
      .duration(1000)
      .attr("x", (d) => xScale(d[0]))
      .attr("y", (d) => yScale(d.data.university))
      .attr("width", (d) => xScale(d[1]) - xScale(d[0]));

    enterBars.merge(bars).on("mouseover", mouseOverFunction);
    enterBars.merge(bars).on("mouseout", mouseOutFunction);

    layersGroup.exit().remove();
    bars.exit().remove();
  }

  return stackedBarchartObject;
}
