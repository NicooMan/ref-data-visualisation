/*-------------------------------------------------------------------- 
  
   Module: piechart class implemented in Bostock's functional style
   Author: Mike Chantler
  
   What it does:
  	Renders a pie chart using the GUP
  
   Dependencies
  	D3.js v4
  
   Version history
  	v001	17/09/2017	mjc	Created.
	  v002	17/08/2018	mjc	Non-linear tweening added
    v003	26/09/2018	mjc Tidy to make classes more similar to barchart with axes module
    v004              nn149 Legend added
  
---------------------------------------------------------------------- */

function piechart(targetDOMelement) {
  //Here we use a function declaration to imitate a 'class' definition
  //
  //Invoking the function will return an object (piechartObject)
  //    e.g. piechart_instance = piechart(target)
  //    This also has the 'side effect' of appending an svg to the target element
  //
  //The returned object has attached public and private methods (functions in JavaScript)
  //For instance calling method 'updateAndRenderData()' on the returned object
  //(e.g. piechart_instance) will render a piechart to the svg

  //Delare the main object that will be returned to caller
  var piechartObject = {};

  //=================== PUBLIC FUNCTIONS =========================
  //
  piechartObject.overrideDataFieldFunction = function(dataFieldFunction) {
    dataField = dataFieldFunction;
    return piechartObject;
  };

  piechartObject.overrideMouseOverFunction = function(callbackFunction) {
    mouseOverFunction = callbackFunction;
    layoutAndRender();
    return piechartObject;
  };

  piechartObject.overrideMouseOutFunction = function(callbackFunction) {
    mouseOutFunction = callbackFunction;
    layoutAndRender();
    return piechartObject;
  };

  piechartObject.render = function() {
    layoutAndRender();
    return piechartObject;
  };

  piechartObject.loadAndRenderDataset = function(data) {
    dataset = data;
    layoutAndRender();
    return piechartObject;
  };
  piechartObject.loadColorPalette = function(colors) {
    colorPalette = colors;
  };

  //=================== PRIVATE VARIABLES ====================================
  var dataset = [];
  var svgWidth = 400;
  var svgHeight = 400;
  var margin = 20;
  var colorPalette = ["#E54229", "#FF9728", "#9BC53D", "#FDE74C", "#5BC0EB"];
  var color = d3
    .scaleOrdinal()
    .domain(["4*", "3*", "2*", "1*", "unclassified"])
    .range(colorPalette);
  var legendRectSize = 18;
  var legendSpacing = 4;

  //=================== INITIALISATION CODE ====================================

  //Declare and append SVG element
  var svg = d3
    .select(targetDOMelement)
    .append("svg")
    .attr("width", svgWidth)
    .attr("height", svgHeight)
    // .attr("width", width + margin.left + margin.right)
    // .attr("height", height + margin.top + margin.bottom)
    .classed("piechart", true);

  //Declare and append group that we will use tp center the piechart within the svg
  var grp = svg.append("g");

  //=================== PRIVATE FUNCTIONS ====================================

  var dataField = function(d) {
    return d.dataField;
  };
  var GUPkeyField = function(d) {
    return d.data.keyField;
  };

  var mouseOverFunction = function(d, i) {
    d.data.highlight = "highlight";
    layoutAndRender();
  };
  var mouseOutFunction = function(d, i) {
    d.data.highlight = "noHighlight";
    layoutAndRender();
  };

  //Set up shape generator
  var arcShapeGenerator = d3
    .arc()
    .outerRadius(svgHeight / 2 - margin)
    .innerRadius(svgHeight / 4)
    .padAngle(0.03)
    .cornerRadius(8);

  function layoutAndRender() {
    //Generate the layout
    var arcsLayout = d3
      .pie()
      .value(dataField)
      .sort(null)(dataset);

    //center the group within the svg
    grp.attr(
      "transform",
      "translate(" + svgWidth / 2 + "," + svgHeight / 2 + ")"
    );
    //add the color palette to the dataset
    for (var i = 0; i < dataset.length; i++) {
      dataset[i].color = colorPalette[i];
    }

    //Now call the GUP
    GUP_legend(dataset);
    GUP_pies(arcsLayout, arcShapeGenerator);
  }

  function GUP_legend(dataset) {
    var selLegend = grp
      .selectAll(".legend")
      // .data(color.domain())
      .data(dataset);

    //Enter selection legend
    var enterSelLegend = selLegend
      .enter()
      .append("g")
      .attr("class", "legend")
      .classed("enter selection", true)
      .attr("transform", function(d, i) {
        // NEW
        var height = legendRectSize + legendSpacing;
        var offset = (height * dataset.length) / 2;
        var horz = -2 * legendRectSize - 15;
        var vert = i * height - offset;
        return "translate(" + horz + "," + vert + ")";
      });

    //Append color rect
    enterSelLegend
      .append("rect")
      .attr("width", legendRectSize)
      .attr("height", legendRectSize)
      .style("fill", function(d) {
        return d.color;
      });

    // Append legend label
    enterSelLegend
      .append("text")
      .text(function(d) {
        return d.keyField + " : " + d.dataField + " %";
      })
      .attr("x", legendRectSize + legendSpacing)
      .attr("y", legendRectSize - legendSpacing);

    //update selection legend
    selLegend.select("text").text(function(d) {
      return d.keyField + " : " + d.dataField + " %";
    });

    //exit selection legend
    selLegend.exit().remove();

    // enterSelLegend
    //   .transition(1000)
    //   .duration(1000)
    //   .delay(function(d, i) {
    //     return 1300 + 100 * i;
    //   });
  }

  function GUP_pies(arcsLayout, arcShapeGenerator) {
    //GUP = General Update Pattern to render pies

    //GUP: BIND DATA to DOM placeholders
    var selection = grp.selectAll("path").data(arcsLayout, GUPkeyField);

    //PIE GUP: ENTER SELECTION
    var enterSel = selection
      .enter()
      .append("path")
      .each(function(d) {
        this.dPrevious = d;
      }); // store d for use in tweening

    enterSel //Add CSS classes
      .attr("class", d => "key--" + GUPkeyField(d))
      .classed("classPie", true)
      .classed("arcs enterSelection", true)
      .classed("highlight", d => d.highlight);

    //Add a tooltip
    enterSel.append("title").text(function(d, i) {
      return JSON.stringify(d.data);
    });

    //GUP UPDATE selection
    var updateSel = selection //update CSS classes
      .classed("updateSelection", true)
      .classed("highlight enterSelection exitSelection", false)
      .classed("highlight", d => d.highlight);

    updateSel
      .select("title") //
      .text(function(d, i) {
        return JSON.stringify(d.data);
      });

    //GUP ENTER AND UPDATE selection
    //Merge enter and update selections into one selection
    var mergedSel = enterSel.merge(selection);

    mergedSel
      .style("fill", function(d) {
        return color(GUPkeyField(d));
      })
      .on("mouseover", mouseOverFunction)
      .on("mouseout", mouseOutFunction);

    mergedSel
      .transition()
      .duration(750)
      .attrTween("d", arcTween); //Use custom tween to draw arcs

    //Now filter selections to set or reset highlight classes
    //as alternative highlighting method
    mergedSel
      .filter(function(d) {
        return d.data.highlight == "highlight";
      })
      .classed("highlight", true)
      .classed("noHighlight", false);

    mergedSel
      .filter(function(d) {
        return d.data.highlight == "noHighlight";
      })
      .classed("highlight", false)
      .classed("noHighlight", true);

    //GUP EXIT selection
    selection
      .exit()
      .classed("highlight updateSelection enterSelection", false)
      .classed("exitSelection", true)
      .remove();
  }

  //Ignore this function unless you really want to know how interpolators work
  function arcTween(dNew) {
    //Create the linear interpolator function
    //this provides a linear interpolation of the start and end angles
    //stored 'd' (starting at the previous values in 'd' and ending at the new values in 'd')
    var interpolateAngles = d3.interpolate(this.dPrevious, dNew);
    //Now store new d for next interpoloation
    this.dPrevious = dNew;
    //Return shape (path for the arc) for time t (t goes from 0 ... 1)
    return function(t) {
      return arcShapeGenerator(interpolateAngles(t));
    };
  }

  //================== IMPORTANT do not delete ==================================
  return piechartObject; // return the main object to the caller to create an instance of the 'class'
} //End of piechart() declaration
