/**
 * 
 * Takes the formatted scatter data and outputs the data to a scatter graph 
 * Schatter chart is adapted from http://bl.ocks.org/kavyashreesiri/cdd945018f1fee59c8da5ebda5f399ba
 * 
 * Also renders tables of the topic weights to compare overall top topics for uoa top topics for selected uni and 
 * top topics for top scoring uni
 * 
 * tables adapted from
 */


function renderScatterPlot(scatterData) {
    // TODO: convert this into a set up method/function and then an update method/function
    // class the circles to include selected uni top uni and then add the tooltips


    d3.select('.scatterPlot')
        .remove()
        .exit();

    // Plot the scatter chart
    var scatterWidth = width;
    scatterPlt = d3.select("#wordcountVsScore")
        .append("svg")
        .attr('class', 'scatterPlot')
        .attr("width", (scatterWidth + margin.left + margin.right))
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    var xScale = d3.scaleLinear().range([0, scatterWidth]),
        xAxis = d3.axisBottom(xScale)

    var yScale = d3.scaleLinear().range([height, 0]), // value -> display
        yAxis = d3.axisLeft(yScale);

    xScale.domain(d3.extent(scatterData, function (d) { return d.wordcount; }));
    yScale.domain([0, d3.max(scatterData, function (d) { return d.score; })]);

    scatterPlt.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)

        .attr("class", "label")
        .attr("x", scatterWidth)
        .attr("y", -6)
        .style("text-anchor", "end")
        ;

    // y-axis
    scatterPlt.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("4* Submissions");

    scatterPlt
        .append('text')
        .attr("transform", "translate(-35," + (height + margin.bottom) / 2 + ") rotate(-90)")
        .text("Number of 4* Submissions");
    scatterPlt
        .append("text")
        .attr("transform", "translate(" + (scatterWidth / 2) + "," + (height + margin.bottom - 5) + ")")
        .text("WordCount");



    // draw dots
    scatterPlt.selectAll(".dot")
        .data(scatterData)
        .enter()
        .append("circle")
        .attr("class", d => {
            var classStr = "dot";
            if (d.key.trim() == selectedUniName)
                classStr += " selectedUni";
            else
                classStr += " normalUni";
            var topScore = calculateTopUni()
            if (d.score == topScore)
                classStr += " topUni"
            classStr += " key--" + d.key.replace(/[ ,'.]/g, '')
            return classStr
        })
        .attr("r", 4)
        .attr("cx", function (d) { return xScale(d.wordcount) })
        .attr("cy", function (d) { return yScale(d.score) })
        .on("mouseover", function (d) {
            d3.selectAll(".key--" + d.key.replace(/[ ,'.]/g, ''))
                .classed('highlight', true)

            var s = findFromKey(uoaAll, d.key)
            var k = s.values[0]['Institution name']
            var needle = "Unranked";
            worldRankings.forEach(function (wr) {
                var r;
                if (k == wr.University) {
                    needle = wr.Score_Rank
                }

            })
            div
                .style("left", d3.event.pageX - 100 + "px")
                .style("top", d3.event.pageY - 150 + "px")
                .style("display", "inline-block")
                .html(d.key + "<br>" + "4* Entries: " + d.dataField +
                    "<br>" + "3* Entries: " + s['values'][0]['overall']['3*'] +
                    "<br>" + "2* Entries: " + s['values'][0]['overall']['2*'] +
                    "<br>" + "1* Entries: " + s['values'][0]['overall']['1*'] +
                    "<br>" + "Unclassified Entries: " + s['values'][0]['overall']['unclassified'] +
                    "<br>" + "World Ranking: " + needle
                );
        })
        .on("mouseout", function (d) {
            d3.selectAll(".key--" + d.key.replace(/[ ,'.]/g, ''))
                .classed('highlight', false)

            div.style("display", "none");
        })

}

/**
* 
* Function that formats the data for the scatter plot comparison of wordcount and 
* score to visualiser any correlation
* 
*/
function formatForScatterWC(data) {
    /*
    Function to format the REF dataset for a scatter plot comparing 4* scores 
    vs WordCount. 

    add in the topicsAsArray 
    */
    console.log(data)
    var scatterData = [];

    data.forEach(function (d) {
        var dict = {};
        dict["key"] = d.values["0"]["Institution name"];
        dict["wordcount"] = d.values[0].environment.WordCount;
        dict["score"] = d.values[0].outputs["4*"];
        dict['topics'] = d.values[0].environment.topicsAsArray;
        scatterData.push(dict);
    })



    return scatterData;
}

/**
 * 
 * Calculates the top topics for the unit of assessment selected
 * This is used for displaying tables of the top topic weights for the selected UoA
 * 
 */

function calculateTopTopics(data) {
    var heirarchy = [];
    var topTopics = {};
    data.forEach(function (d) {
        d.topics.forEach(function (t) {
            if (!(t.topicAs3words in topTopics)) {
                topTopics[t.topicAs3words] = 0
            }
            topTopics[t.topicAs3words] += t.weight
        })
    })
    Object.keys(topTopics).forEach(function (d) {
        if (topTopics[d] > 0.05) {

            var payload = {}

            payload.name = d
            payload.value = (topTopics[d] / Object.keys(data).length).toFixed(3)

            heirarchy.push(payload)
        }
    })
    heirarchy = heirarchy.sort(function (a, b) {
        return b.value - a.value
    })


    return heirarchy
}

/**
 * 
 * A function for finding the topic data relating to the key
 * this can be a selected uni or the top uni. This function
 * clips the data and removes any data equal to 0
 */
function getTopicsForUniandUoA(data, key) {
    var needle;
    var out = [];
    data.forEach(function (d) {
        if (d.key == key)
            needle = d.topics
    })

    needle.forEach(function (d) {

        if (d.weight > 0) {
            var payload = {}

            payload.name = d.topicAs3words
            payload.value = d.weight.toFixed(3)

            out.push(payload)
        }
    })
    out = out.sort(function (a, b) {
        return b.value - a.value
    })
    return out
}