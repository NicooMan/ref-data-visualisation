/*-------------------------------------------------------------------- 
  
   Module: simple map class implemented in Bostock's functional style
   Author: Mike Chantler
  
   What it does:
  	Renders a bar chart in as simple mannert as posibe
  
   Dependencies
  	D3.js v4
  
   Version history
  	v001	29/08/2018	mjc	Created.
  
---------------------------------------------------------------------- */
"use safe";

function map(targetDOMelement) {
  //Here we use a function declaration to imitate a 'class' definition
  //
  //Invoking the function will return an object (mapObject)
  //    e.g. map_instance = map(target)
  //    This also has the 'side effect' of appending an svg to the target element
  //
  //The returned object has attached public and private methods (functions in JavaScript)
  //For instance calling method 'updateAndRenderData()' on the returned object
  //(e.g. map_instance) will render a map to the svg

  //Delare the main object that will be returned to caller
  var mapObject = {};

  //=================== PUBLIC FUNCTIONS =========================
  //

  mapObject.loadAndRenderMap = function (countries) {
    topojsonCountries = countries;
    GUP_countries(countriesGrp, topojsonCountries);
    return mapObject;
  };
  mapObject.loadAndRenderTowns = function (towns) {
    topojsonTowns = towns;
    GUP_towns(townsGrp, topojsonTowns);
    return mapObject;
  };
  mapObject.overrideTownLongLatAccessor = function (functionRef) {
    longLatAccessor = functionRef;
    return mapObject;
  };
  mapObject.overrideTownNameAccessor = function (functionRef) {
    townNameAccessor = functionRef;
    return mapObject;
  };

  mapObject.overrideDatafieldAccessor = function (functionRef) {
    dataFieldAccessor = functionRef;
    return mapObject;
  };

  mapObject.appendClickFunction = function (callbackFunction) {
    appendedClickFunction = callbackFunction;
    return mapObject;
  };

  //=================== PRIVATE VARIABLES ====================================
  //Width and height of svg canvas
  var width = 960,
    height = 1160,
    centered;
  var zoomFactor = 1;
  var topojsonCountries, topojsonTowns;
  var targetDOM = targetDOMelement;
  var countries, towns;

  //=================== INITIALISATION CODE ====================================

  //Declare and append SVG element
  //Create SVG
  var svg = d3
    .select(targetDOMelement)
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .classed("map", true);

  //Set up separate town/country groups for clarity
  var mapGrp = svg.append("g").classed("mapGrp", true);
  var countriesGrp = mapGrp.append("g").classed("countriesGrp", true);
  var townsGrp = mapGrp.append("g").classed("townsGrp", true);

  //===================== PRIVATE FUNCTIONS =========================================

  var townNameAccessor = (d) => d.properties.name; //Default town name in topojson format
  var gupKey = (d) => "key--" + townNameAccessor(d).replace(/[\W]+/g, "_"); //Add "key--" and replace nasty spaces etc with underscores
  var longLatAccessor = (d) => d.geometry.coordinates; //Default latitude, longitude is geojson
  var town_xyPosition = (d) =>
    "translate(" + projection(longLatAccessor(d)) + ")";
  var dataFieldAccessor = (d) => d; //default sizeAccessor
  var sizeCircle = (d) => dataFieldAccessor(d) / 2;

  var appendedClickFunction = function (d) {};

  var tooltip = function (d) {
    return (
      d["Institution name"] +
      "\n" +
      d["UoAString"] +
      `\nOverall 4*: ${d.overall["4*"]}%`
    );
  };

  //define projection of spherical coordinates to the Cartesian plane
  var projection = d3
    .geoAlbers()
    .center([0, 55.4])
    .rotate([4.4, 0])
    .parallels([50, 60])
    .scale(1200 * 5)
    .translate([width / 2, height / 2]);

  //Define path generator (takes projected 2D geometry and formats for SVG)
  var pathGen = d3.geoPath().projection(projection).pointRadius(2);

  var circleSizeZoom = function (d) {
    var r;
    zoomFactor < 1
      ? (r = dataFieldAccessor(d))
      : (r = dataFieldAccessor(d) / zoomFactor);
    return r / 2;
  };

  var textSizeZoomHighlight = function (d) {
    var r;
    zoomFactor > 3 ? (r = 13 / 3) : (r = 13 / zoomFactor);
    return r;
  };
  var textSizeZoomNoHighlight = function (d) {
    var r;
    zoomFactor > 3 ? (r = 10 / 3) : (r = 10 / zoomFactor);
    return r;
  };

  var zoom = d3.zoom().on("zoom", () => {
    mapGrp.attr("transform", d3.event.transform);
    zoomFactor = d3.event.transform.k;
    // d3.select(".classCountry").style("stroke-width", `${1 / zoomFactor}px`);
    // d3.select(".classCountry.highlight").style(
    //   "stroke-width",
    //   `${1.5 / zoomFactor}px`
    // );
    // townsGrp.selectAll("circle").attr("r", circleSizeZoom);
    // townsGrp
    //   .selectAll(".noHighlight text")
    //   .style("font-size", textSizeZoomNoHighlight);
    // console.log(zoomFactor);
  });

  svg.call(zoom);

  //===========================RENDER FUNCTIONS=========================

  function GUP_countries(countriesGrp, countries) {
    //Draw the five unit outlines (ENG, IRL, NIR, SCT, WLS)

    //DATA BIND
    var selection = countriesGrp
      .selectAll(".classCountry")
      .data(countries, (d) => d.id); //Use ENG, IRL etc as key

    //ENTER
    var grp = selection.enter().append("g").attr("class", "classCountry");

    var enterSel = grp
      .append("path")
      .attr("class", (d) => "key--" + d.id)
      .classed("classCountry", true)
      .attr("d", pathGen);

    grp
      .append("text")
      .text((d) => d.id)
      .attr("x", (d) => pathGen.centroid(d)[0])
      .attr("y", (d) => pathGen.centroid(d)[1]);

    //ENTER + UPDATE
    enterSel
      .merge(selection)
      .on("mouseover", function (d, i) {
        d3.select(this).classed("highlight", true);
      })
      .on("mouseout", function (d, i) {
        d3.select(this).classed("highlight", false);
      });
    // .on("click", clicked2);

    //EXIT
    selection.exit().remove();
  }

  function GUP_towns(townsGrp, towns) {
    //DATA BIND
    var selection = townsGrp.selectAll("g.classTown").data(towns, gupKey);

    //ENTER
    var enterSelection = selection
      .enter()
      .append("g")
      .attr("class", gupKey)
      .classed("classTown", true)
      .classed("enterSelection", true)
      .classed("noHighlight", true)
      .on("mouseover", function (d) {
        console.log("d=", d);
      })
      .attr("transform", town_xyPosition);

    //Append circles
    enterSelection
      .append("circle")
      .transition()
      .duration(1000)
      .attr("r", sizeCircle);

    //Append labels
    enterSelection.append("text").text(townNameAccessor);

    enterSelection.append("title").text(tooltip);
    console.log("here", towns);

    var updateSelection = selection
      .classed("enterSelection", false)
      .classed("updateSelection", true);

    updateSelection
      .select("circle")
      .transition()
      .duration(1000)
      .attr("r", sizeCircle);

    updateSelection.select("text").text(townNameAccessor);
    updateSelection.select("title").text(tooltip);

    //ENTER + UPDATE
    enterSelection
      .merge(selection)
      .on("mouseover", function (d, i) {
        d3.select(this).classed("noHighlight", false);
        d3.select(this).classed("highlight", true);
        townsGrp
          .selectAll(".highlight text")
          .style("font-size", textSizeZoomHighlight);
      })
      .on("mouseout", function (d, i) {
        d3.select(this).classed("noHighlight", true);
        d3.select(this).classed("highlight", false);
        townsGrp
          .selectAll(".noHighlight text")
          .style("font-size", textSizeZoomNoHighlight);
      })
      .on("click", appendedClickFunction);

    //EXIT
    var exitSelection = selection.exit().remove();

    // var circleExitSelection = selection
    //   .select("circle")
    //   .exit()
    //   .transition()
    //   .duration(1000)
    //   .attr("r", 0)
    //   .remove();

    // var textExitSelection = selection
    //   .select("text")
    //   .exit()
    //   .text("")
    //   .remove();

    // circleExitSelection.merge(textExitSelection).exit().remove();

    // exitSeleciton
    //   .select("text")
    //   // .transition()
    //   // .duration(1000)
    //   .remove();

    // exitSelection.remove();
  }

  //==============================INTERACTION FUNCTIONS==============================

  function clicked2(d) {
    var centroid = pathGen.centroid(d),
      translate = projection.translate();

    projection.translate([
      translate[0] - centroid[0] + width / 2,
      translate[1] - centroid[1] + height / 2,
    ]);

    // zoom.translate(projection.translate());

    countriesGrp
      .selectAll("path")
      .transition()
      .duration(700)
      .attr("d", pathGen);
  }

  function clicked(d) {
    console.log(d);
    var x, y, k;

    if (d && centered !== d) {
      var centroid = pathGen.centroid(d);
      x = centroid[0];
      y = centroid[1];
      k = 4;
      centered = d;
    } else {
      x = width / 2;
      y = height / 2;
      k = 1;
      centered = null;
    }

    countriesGrp.selectAll("path").classed(
      "active",
      centered &&
        function (d) {
          return d === centered;
        }
    );

    countriesGrp
      .transition()
      .duration(1000)
      .attr(
        "transform",
        "translate(" +
          width / 2 +
          "," +
          height / 2 +
          ")scale(" +
          k +
          ")translate(" +
          -x +
          "," +
          -y +
          ")"
      )
      .style("stroke-width", 1.5 / k + "px");

    townsGrp.selectAll("g").classed(
      "active",
      centered &&
        function (d) {
          return d === centered;
        }
    );

    townsGrp
      .transition()
      .duration(1000)
      .attr(
        "transform",
        "translate(" +
          width / 2 +
          "," +
          height / 2 +
          ")scale(" +
          k +
          ")translate(" +
          -x +
          "," +
          -y +
          ")"
      )
      .style("stroke-width", 1.5 / k + "px");
  }

  //================== IMPORTANT do not delete ==================================
  return mapObject; // return the main object to the caller to create an instance of the 'class'
} //End of map() declaration
