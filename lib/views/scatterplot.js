"use strict";

function scatterplot(targetDOMelement) {
  var scatterplotObject = {};

  scatterplotObject.loadAndRenderDataset = function (data) {
    dataset = data;
    render();
    return scatterplotObject;
  };

  scatterplotObject.overridexFieldAccessor = function (accessorFunction) {
    xField = accessorFunction;
    return scatterplotObject;
  };

  scatterplotObject.overrideyFieldAccessor = function (accessorFunction) {
    yField = accessorFunction;
    return scatterplotObject;
  };

  var xField = function (d) {
    return d.xField;
  };

  var yField = function (d) {
    return d.yField;
  };

  var tooltip = function (d) {
    return (
      d.university +
      "\n" +
      d.UoA +
      `\nOverall 4*: ${d.overall4ratings}%\nImpact 4*: ${d.impact4ratings}%\nEnvironment 4*: ${d.environment4ratings}%\nOutputs 4* ${d.outputs4ratings}%`
    );
  };

  var mouseOverFunction = function (d) {
    d3.select(this).attr("r", 8);
  };
  var mouseOutFunction = function (d) {
    d3.selectAll(".dots").attr("r", 6);
  };

  function isBrushed(brushCoords, cx, cy) {
    var x0 = brushCoords[0][0],
      x1 = brushCoords[1][0],
      y0 = brushCoords[0][1],
      y1 = brushCoords[1][1];

    return x0 <= cx && cx <= x1 && y0 <= cy && cy <= y1;
  }

  var dataset = [];
  var margin = { top: 50, bottom: 40, left: 20, right: 100 };
  var width = 1000 - margin.left - margin.right;
  var height = 400 - margin.top - margin.bottom;
  var yIndent = 30;

  var svg = d3
    .select(targetDOMelement)
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .classed("scatterplot", true);

  var g = svg
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var xAxis = g
    .append("g")
    .attr("class", "xAxis")
    .attr("transform", "translate(0,+" + height + ")");

  var yAxis = g
    .append("g")
    .attr("class", "yAxis")
    .attr("transform", "translate(" + yIndent + ",0)");

  g.append("text")
    .attr("class", "scatterplot--title")
    .classed("title", true)
    .attr("transform", "translate(" + width / 2 + ",0)")
    .attr("text-anchor", "middle")
    .attr("y", -10)
    .text("Overall 4* % vs Scaled FTE");

  g.append("text")
    .attr("class", "scatterplot--label")
    .classed("xlabel", true)
    .attr("transform", "translate(" + width / 2 + "," + (height + 40) + ")")
    .attr("text-anchor", "middle")
    .text("Scaled FTE");

  g.append("text")
    .attr("class", "scatterplot--label")
    .classed("ylabel", true)
    .attr("transform", "rotate(-90)")
    .attr("text-anchor", "middle")
    .attr("x", -height / 2)
    .text("Overall 4* %");

  var legend = g
    .append("g")
    .attr("class", "legend")
    .selectAll("g")
    .data(["Selected UoA", "Other UoAs", "Map selection"])
    .enter()
    .append("g")
    .attr("transform", function (d, i) {
      return "translate(0," + i * 25 + ")";
    });

  legend
    .append("circle")
    .attr("cx", width)
    .attr("r", 6)
    .attr("fill", function (d) {
      var trans = {
        "Selected UoA": "gold",
        "Other UoAs": "lightskyblue",
        "Map selection": "saddlebrown",
      };
      return trans[d];
    });

  legend
    .append("text")
    .attr("x", width + 10)
    .attr("y", 3)
    .text((d) => d);

  var brushGroup = g.append("g").attr("class", "brushSelection");

  var dotsGroup = g.append("g").attr("class", "dotsGroup");

  function render() {
    var xScale = d3
      .scaleLinear()
      .domain([0, d3.max(dataset.map(xField))])
      .range([yIndent, width - 60]);

    xAxis.transition(1500).call(d3.axisBottom(xScale));

    var yScale = d3
      .scaleLinear()
      .domain([0, d3.max(dataset.map(yField))])
      .range([height, 0]);
    yAxis.transition(1500).call(d3.axisLeft(yScale));

    var brush = d3.brush().on("brush", selectCircles);

    // brushGroup.call(brush);

    var dots = dotsGroup.selectAll(".dots").data(dataset);

    var enterDots = dots.enter().append("circle");

    enterDots
      .attr("class", "dots")
      .attr("id", (d) => d.university.replace(/\s/g, "-"))
      .attr("uoa", (d) => d.UoA)
      .attr("cx", (d) => xScale(xField(d)))
      .attr("cy", (d) => yScale(yField(d)))
      .attr("r", 6);

    enterDots
      .merge(dots)
      .on("mouseover", mouseOverFunction)
      .on("mouseout", mouseOutFunction);

    enterDots.append("title").text(tooltip);

    var updateSel = dots;

    updateSel
      .transition(1500)
      .attr("id", (d) => d.university.replace(/\s/g, "-"))
      .attr("uoa", (d) => d.UoA)
      .attr("cx", (d) => xScale(xField(d)))
      .attr("cy", (d) => yScale(yField(d)));

    updateSel.select("title").text(tooltip);

    dots.exit().remove();

    function selectCircles() {
      if (d3.event.selection != null) {
        var dots = dotsGroup.selectAll(".dots");
        // var dots = d3.selectAll(".dots");
        dots.classed("brushed", false);

        var brushCoords = d3.brushSelection(this);

        dots
          .filter(function (d) {
            var cx = d3.select(this).attr("cx"),
              cy = d3.select(this).attr("cy");

            return isBrushed(brushCoords, cx, cy);
          })
          .classed("brushed", true);
      }
    }
  }

  return scatterplotObject;
}
