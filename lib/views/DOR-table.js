/**
 * 
 * A function for rendering the topic data of the UoA, the selected uni and the top uni
 * Code adapted from:
 * https://bl.ocks.org/denisemauldin/df41a0ec91f0d9697b03651b2238a0a0
 */

function renderTopicTables(scatterData) {

    d3.selectAll('.topics')
        .remove()
        .exit()
        .data(scatterData);

    d3.select('#topTopics')
        .html(`<table class='topics'> <tr valign="top">
            <td>
                <h4>Average Topics weights For UoA</h4>
                <table class="topTopicsForUoA">
                    <thead>
                        <tr>
                            <th>Topic</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </td>
            <td>
                <h4>Top Topics For Selected Uni</h4>
                <table class="topTopicsForUni">
                    <thead>
                        <tr>
                            <th>Topic</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </td>
            <td>
                <h4>Top Topics For Top Uni</h4>
                <table class="topTopicsForTopUni">
                    <thead>
                        <tr>
                            <th>Topic</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </td>
        </tr>
    </table>`)
    var sortedTopTopics = calculateTopTopics(scatterData)

    // plot tables to show topics relevant to the data

    var tr = d3.select(".topTopicsForUoA tbody")
        .selectAll("tr")
        .data(sortedTopTopics)
        .enter().append("tr");

    var td = tr.selectAll("td")
        .data(function (d, i) {
            return Object.values(d);
        })
        .enter().append("td")
        .text(function (d) {
            return d;
        })
        .attr("class", "tabledata");

    var selectedUniTopics = getTopicsForUniandUoA(scatterData, selectedUniName)

    var tr = d3.select(".topTopicsForUni tbody")
        .selectAll("tr")
        .data(selectedUniTopics)
        .enter().append("tr");

    var td = tr.selectAll("td")
        .data(function (d, i) {
            return Object.values(d);
        })
        .enter().append("td")
        .text(function (d) {
            return d;
        })
        .attr("class", "tabledata");

    var selectedUniTopics = getTopicsForUniandUoA(scatterData, topUni[0].key)

    var tr = d3.select(".topTopicsForTopUni tbody")
        .selectAll("tr")
        .data(selectedUniTopics)
        .enter().append("tr");

    var td = tr.selectAll("td")
        .data(function (d, i) {
            return Object.values(d);
        })
        .enter().append("td")
        .text(function (d) {
            return d;
        })
        .attr("class", "tabledata");
}