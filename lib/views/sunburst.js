function sunburst(targetDOMelement) {
  var sunburstObject = {};

  //------------------------------------Public Functions--------------------------
  sunburstObject.loadAndRenderDataset = function (data) {
    dataset = data;
    render();
  };

  //------------------------------------Private Functions--------------------------
  var arcReveal = function (d) {
    return d.y1 <= 2 && d.y0 >= 1 && d.x1 > d.x0;
  };

  var labelReveal = function (d) {
    return d.y1 <= 2 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
  };

  var labelTransform = function (d) {
    const x = (((d.x0 + d.x1) / 2) * 180) / Math.PI;
    const y = ((d.y0 + d.y1) / 2) * radius;
    return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
  };

  var splitLabel = function (label) {
    return (text = label
      .split("-")
      .map((d) => d.charAt(0).toUpperCase() + d.slice(1)));
  };

  var mouseOverFunction = function (d) {
    if (arcReveal(d.current)) {
      d3.select(this).attr("fill-opacity", 0.9);
    }

    if (d.depth === 1) {
      renderMainTopicName(d, false);
    }

    if (d.depth === 2) {
      renderSecondTopicNameAndSimilarity(d);
      console.log(d);
    }
  };

  var mouseOutFunction = function (d) {
    if (arcReveal(d.current)) {
      d3.select(this).attr("fill-opacity", 0.6);
      d3.selectAll(".mainTopic").selectAll(".mainTopicLables.hovered").remove();
      d3.selectAll(".secondTopic").remove();
      d3.selectAll(".similarityValue").remove();
    }
  };

  //------------------------------------Private Variables--------------------------

  var dataset = [];
  var margin = { top: 20, bottom: 20, left: 20, right: 20 };
  var width = 700 - margin.left - margin.right;
  var height = 700 - margin.top - margin.bottom;
  var radius = width / 4;
  var color = d3.scaleOrdinal(d3.schemeCategory20);

  //------------------------------------Initialisation Code--------------------------

  var svg = d3
    .select(targetDOMelement)
    .append("svg")
    .attr("width", width + margin.top + margin.bottom)
    .attr("height", height + margin.left + margin.right)
    .classed("sunburst", true);

  var g = svg
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  //------------------------------------Render--------------------------
  function render() {
    partition = (data) => {
      var root = d3.hierarchy(data).sum((d) => d.value);
      return d3.partition().size([2 * Math.PI, root.height + 1])(root);
    };

    var arc = d3
      .arc()
      .startAngle((d) => d.x0)
      .endAngle((d) => d.x1)
      .padAngle((d) => Math.min((d.x1 - d.x0) / 2, 0.005))
      .padRadius(radius * 1.5)
      .innerRadius((d) => d.y0 * radius)
      .outerRadius((d) => Math.max(d.y0 * radius, d.y1 * radius - 1));

    var root = partition(dataset);
    root.each((d) => (d.current = d));

    var path = g
      .append("g")
      .selectAll("path")
      .data(root.descendants().slice(1))
      .enter()
      .append("path")
      .attr("fill", (d) => {
        while (d.depth > 1) d = d.parent;
        return color(d.data.name);
      })
      .attr("fill-opacity", (d) => (arcReveal(d.current) ? 0.6 : 0))
      .attr("d", (d) => arc(d.current));

    path
      .filter((d) => d.children)
      .style("cursor", "pointer")
      .on("click", onClickFunction);

    path
      .filter((d) => arcReveal(d.current))
      .style("cursor", "pointer")
      .on("mouseover", mouseOverFunction)
      .on("mouseout", mouseOutFunction);

    var label = g
      .append("g")
      .attr("pointer-events", "none")
      .attr("text-anchor", "middle")
      .style("user-select", "none")
      .selectAll("text")
      .data(root.descendants().slice(1))
      .enter()
      .append("text")
      .attr("font-size", "8px")
      .attr("dy", "0.35em")
      .attr("fill-opacity", (d) => +labelReveal(d.current))
      .attr("transform", (d) => labelTransform(d.current))
      .text((d) => splitLabel(d.data.name).join("-"));

    var parent = g
      .append("circle")
      .datum(root)
      .attr("r", radius)
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .on("click", function (p) {
        onClickFunction(p);
        d3.selectAll(".mainTopic").remove();
        path.filter((d) => !arcReveal(d.target)).attr("pointer-events", "none");
      });

    //Function inspired and adapted from https://observablehq.com/@d3/zoomable-sunburst
    function onClickFunction(p) {
      renderMainTopicName(p, true);
      parent.datum(p.parent || root);

      root.each(
        (d) =>
          (d.target = {
            x0:
              Math.max(0, Math.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) *
              2 *
              Math.PI,
            x1:
              Math.max(0, Math.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) *
              2 *
              Math.PI,
            y0: Math.max(0, d.y0 - p.depth),
            y1: Math.max(0, d.y1 - p.depth),
          })
      );

      var t = g.transition().duration(750);

      path
        .transition(t)
        .tween("data", (d) => {
          const i = d3.interpolate(d.current, d.target);
          return (t) => (d.current = i(t));
        })
        .filter(function (d) {
          return +this.getAttribute("fill-opacity") || arcReveal(d.target);
        })
        .attr("fill-opacity", (d) => (arcReveal(d.target) ? 0.6 : 0))
        .attrTween("d", (d) => () => arc(d.current));

      path
        .filter(function (d) {
          return +this.getAttribute("fill-opacity") || arcReveal(d.target);
        })
        .style("cursor", "pointer")
        .attr("pointer-events", "all")
        .on("mouseover", mouseOverFunction)
        .on("mouseout", mouseOutFunction);

      label
        .filter(function (d) {
          return +this.getAttribute("fill-opacity") || labelReveal(d.target);
        })
        .transition(t)
        .attr("fill-opacity", (d) => +labelReveal(d.target))
        .attrTween("transform", (d) => () => labelTransform(d.current));
    }
  }

  function renderMainTopicName(d, selected) {
    var topicLablesGroup = g.selectAll(".mainTopic").data([d]);
    topicLablesGroup.enter().append("g").attr("class", "mainTopic");

    var labels = g
      .selectAll("g.mainTopic")
      .selectAll("mainTopicLables")
      .data((d) => splitLabel(d.data.name));

    labels
      .enter()
      .append("text")
      .attr("class", "mainTopicLables")
      .attr("text-anchor", "middle")
      .classed("selected", selected)
      .classed("hovered", !selected)
      .attr("fill", color(d.data.name))
      .attr("y", (d, i) => (i - 1) * 20 - radius / 2)
      .text((d) => d);

    labels
      .classed("selected", selected)
      .classed("hovered", !selected)
      .text((d) => d);
  }

  function renderSecondTopicNameAndSimilarity(d) {
    var topicLablesGroup = g.selectAll(".secondTopic").data([d]);
    topicLablesGroup.enter().append("g").attr("class", "secondTopic");

    var labels = g
      .selectAll("g.secondTopic")
      .selectAll("secondTopicLables")
      .data((d) => splitLabel(d.data.name));

    labels
      .enter()
      .append("text")
      .attr("class", "secondTopicLables")
      .attr("text-anchor", "middle")
      .attr("fill", color(d.parent.data.name))
      .attr("y", (d, i) => (i - 1) * 20 + radius / 2)
      .text((d) => d);

    labels.text((d) => d);

    var similarity = g.selectAll(".similarityValue").data([d]);

    similarity
      .enter()
      .append("text")
      .attr("class", "similarityValue")
      .attr("text-anchor", "middle")
      .attr("fill", (d) => color(d.parent.data.name))
      .attr("font-size", "18px")
      .text((d) => (d.data.value * 100).toFixed(1) + "% similarity");

    similarity.text((d) => (d.data.value * 100).toFixed(1) + "% similarity");
  }

  return sunburstObject;
}
