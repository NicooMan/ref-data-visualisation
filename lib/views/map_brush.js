"use safe";

function mapBrush(targetDOMelement) {
  var mapObject = {};

  //=================== PUBLIC FUNCTIONS =========================
  //

  mapObject.loadAndRenderMap = function (countries) {
    topojsonCountries = countries;
    GUP_countries(countriesGrp, topojsonCountries);
    return mapObject;
  };
  mapObject.loadAndRenderTowns = function (towns) {
    topojsonTowns = towns;
    GUP_towns(townsGrp, topojsonTowns);
    return mapObject;
  };
  mapObject.overrideTownLongLatAccessor = function (functionRef) {
    longLatAccessor = functionRef;
    return mapObject;
  };
  mapObject.overrideTownNameAccessor = function (functionRef) {
    townNameAccessor = functionRef;
    return mapObject;
  };

  mapObject.overrideDatafieldAccessor = function (functionRef) {
    dataFieldAccessor = functionRef;
    return mapObject;
  };

  mapObject.appendClickFunction = function (callbackFunction) {
    appendedClickFunction = callbackFunction;
    return mapObject;
  };

  mapObject.appendOnBrushEndFunction = function (callbackFunction) {
    appendedOnBrushEndFunction = callbackFunction;
    return mapObject;
  };

  //=================== PRIVATE VARIABLES ====================================
  //Width and height of svg canvas
  var width = 960,
    height = 1160,
    centered;
  var zoomFactor = 1;
  var topojsonCountries, topojsonTowns;
  var targetDOM = targetDOMelement;
  var countries, towns;

  //=================== INITIALISATION CODE ====================================

  //Declare and append SVG element
  //Create SVG
  var svg = d3
    .select(targetDOMelement)
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .classed("map", true);

  //Set up separate town/country groups for clarity
  var mapGrp = svg.append("g").classed("mapGrp", true);
  var countriesGrp = mapGrp.append("g").classed("countriesGrp", true);
  var townsGrp = mapGrp.append("g").classed("townsGrp", true);
  var brushGroup = mapGrp.append("g").attr("class", "brushSelection");

  //===================== PRIVATE FUNCTIONS =========================================

  var townNameAccessor = (d) => d.properties.name; //Default town name in topojson format
  var gupKey = (d) => "key--" + townNameAccessor(d).replace(/[\W]+/g, "_"); //Add "key--" and replace nasty spaces etc with underscores
  var longLatAccessor = (d) => d.geometry.coordinates; //Default latitude, longitude is geojson
  var town_xyPosition = (d) =>
    "translate(" + projection(longLatAccessor(d)) + ")";
  var dataFieldAccessor = (d) => d; //default sizeAccessor
  var sizeCircle = (d) => dataFieldAccessor(d) / 2.5;

  var appendedClickFunction = function (d) {
    var trans = d3.select(this).attr("transform");
    var regExp = /\(([^)]+)\)/;
    var coord = trans.match(regExp)[1];
    var x = +coord.split(",")[0];
    var y = +coord.split(",")[1];
  };

  function isBrushed(brush_coords, cx, cy) {
    var x0 = brush_coords[0][0],
      x1 = brush_coords[1][0],
      y0 = brush_coords[0][1],
      y1 = brush_coords[1][1];

    return x0 <= cx && cx <= x1 && y0 <= cy && cy <= y1;
  }

  function selectCircles() {
    if (d3.event.selection != null) {
      var townsGroup = d3.selectAll(".classTown");
      var townsGroup = d3.selectAll(".classTown").classed("brushed", false);

      var brush_coords = d3.brushSelection(this);

      townsGroup
        .filter(function (d) {
          var trans = d3.select(this).attr("transform");
          var regExp = /\(([^)]+)\)/;
          var coord = trans.match(regExp)[1];
          var x = +coord.split(",")[0];
          var y = +coord.split(",")[1];

          return isBrushed(brush_coords, x, y);
        })
        .classed("brushed", true);
    }
  }

  function onBrushEnd() {
    var selectedData = d3.selectAll(".brushed").data();
    appendedOnBrushEndFunction(selectedData);
  }

  function appendedOnBrushEndFunction() {}

  //define projection of spherical coordinates to the Cartesian plane
  var projection = d3
    .geoAlbers()
    .center([0, 55.4])
    .rotate([4.4, 0])
    .parallels([50, 60])
    .scale(1200 * 5)
    .translate([width / 2, height / 2]);

  //Define path generator (takes projected 2D geometry and formats for SVG)
  var pathGen = d3.geoPath().projection(projection).pointRadius(2);

  //===========================RENDER FUNCTIONS=========================

  function GUP_countries(countriesGrp, countries) {
    //Draw the five unit outlines (ENG, IRL, NIR, SCT, WLS)

    //DATA BIND
    var selection = countriesGrp
      .selectAll(".classCountry")
      .data(countries, (d) => d.id); //Use ENG, IRL etc as key

    //ENTER
    var grp = selection.enter().append("g").attr("class", "classCountry");

    var enterSel = grp
      .append("path")
      .attr("class", (d) => "key--" + d.id)
      .classed("classCountry", true)
      .attr("d", pathGen);

    grp
      .append("text")
      .text((d) => d.id)
      .attr("x", (d) => pathGen.centroid(d)[0])
      .attr("y", (d) => pathGen.centroid(d)[1]);

    //ENTER + UPDATE
    enterSel
      .merge(selection)
      .on("mouseover", function (d, i) {
        d3.select(this).classed("highlight", true);
      })
      .on("mouseout", function (d, i) {
        d3.select(this).classed("highlight", false);
      });
    // .on("click", clicked2);

    //EXIT
    selection.exit().remove();
  }

  function GUP_towns(townsGrp, towns) {
    //DATA BIND

    var brush = d3.brush().on("brush", selectCircles).on("end", onBrushEnd);

    brushGroup.call(brush);

    var selection = townsGrp.selectAll("g.classTown").data(towns, gupKey);

    //ENTER
    var enterSelection = selection
      .enter()
      .append("g")
      .attr("class", gupKey)
      .classed("classTown", true)
      .classed("enterSelection", true)
      .classed("noHighlight", true)
      .attr("transform", town_xyPosition);

    //Append circles
    enterSelection
      .append("circle")
      .transition()
      .duration(1000)
      .attr("r", sizeCircle);

    //Append labels
    enterSelection.append("text").text(townNameAccessor);

    var updateSelection = selection
      .classed("enterSelection", false)
      .classed("updateSelection", true);

    updateSelection
      .select("circle")
      .transition()
      .duration(1000)
      .attr("r", sizeCircle);

    updateSelection.select("text").text(townNameAccessor);

    //ENTER + UPDATE
    enterSelection
      .merge(selection)
      .on("mouseover", function (d, i) {
        d3.select(this).classed("noHighlight", false);
        d3.select(this).classed("highlight", true);
        townsGrp.selectAll(".highlight text");
      })
      .on("mouseout", function (d, i) {
        d3.select(this).classed("noHighlight", true);
        d3.select(this).classed("highlight", false);
        townsGrp.selectAll(".noHighlight text");
      })
      .on("click", appendedClickFunction);

    var exitSelection = selection.exit().remove();
  }

  //================== IMPORTANT do not delete ==================================
  return mapObject; // return the main object to the caller to create an instance of the 'class'
} //End of map() declaration
