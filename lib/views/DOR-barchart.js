/**
         * Function that creates the bar chart ready for displaying data to the user 
         * upon selected options
         * 
         * chart format is taken from https://bl.ocks.org/d3noob/bdf28027e0ce70bd132edc64f1dd7ea4
         * and adapted accordingly. 
         * 
         */
function setUpBarChart() {
    margin = {
        top: 20,
        right: 150,
        bottom: 100,
        left: 100
    },
        //create width based on the size of the browser window and height of 500 
        width = document.body.clientWidth - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    barChart = d3.select("#compareBarChart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // scale the axes to fit the defined width and height of the chart
    xChart = d3.scaleBand()
        .range([0, width])
        .padding(0.1);
    yChart = d3.scaleLinear()
        .range([height, 0]);

    xAxis = d3.axisBottom(xChart);
    yAxis = d3.axisLeft(yChart);


    // Set up y axis
    barChart.append("g")
        .attr("class", "yaxis")
        .call(yAxis)

    // set up x axis
    barChart.append("g")
        .attr("class", "xaxis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", function (d) {
            return "rotate(-90)";
        });
    barChart
        .append("text")
        .attr("transform", "translate(-35," + (height + margin.bottom) / 2 + ") rotate(-90)")
        .text("Number of 4* Submissions");

    barChart
        .append("text")
        .attr("transform", "translate(" + (width / 2) + "," + (height + margin.bottom - 5) + ")")
        .text("Universities");
}


/**
 * 
 * A function that renders the bar chart with the provided data. This was mainly adapted from:
 * https://bl.ocks.org/d3noob/bdf28027e0ce70bd132edc64f1dd7ea4
 * https://bl.ocks.org/seemantk/ec245e1f4e824e685982dd5d3fbb2fcc 
 * 
 * For visualising the users selected university and the top scoring uni css tags are added 
 * to each bar to make it easier for the user to understand
 * 
 * Cross chart highlighting was adapted from the lab examples
 * 
 * Tool tip also has world ranking data found using additional data from https://www.kaggle.com/joeshamen/world-university-rankings-2020/data
 */
function renderUoaBar(barData) {
    //select all bars
    xChart.domain(barData.map(function (d) {
        return d.key.match(/\b(\w{4})/g).join("");
    }));
    yChart.domain([0, d3.max(barData, function (d) {
        return d.dataField;
    })]);
    //enter
    bar = barChart.selectAll(".bar")
        .remove()
        .exit()
        .data(barData)
    bar
        .enter().append("rect")
        .attr("class", d => {
            var classStr = "bar";
            if (d.key.trim() == selectedUniName)
                classStr += " selectedUni";
            else
                classStr += " normalUni";
            var topScore = calculateTopUni()
            if (d.dataField == topScore)
                classStr += " topUni"

            classStr += " key--" + d.key.replace(/[ ,'.]/g, '')
            return classStr
        })
        // make the x ticks smaller for nicer formatting as full length takes up too much space 
        .attr("x", function (d) {
            return xChart(d.key.match(/\b(\w{4})/g).join(""));
        })
        .attr("width", xChart.bandwidth())
        .attr("y", function (d) {
            return yChart(d.dataField);
        })
        .attr("height", function (d) {
            return height - yChart(d.dataField);
        })
        // selects the tool tip div and fills with relevant data
        // code adapted from https://bl.ocks.org/alandunning/274bf248fd0f362d64674920e85c1eb7
        .on("mousemove", function (d) {
            var s = findFromKey(uoaAll, d.key)
            var k = s.values[0]['Institution name']
            var needle = "Unranked";
            worldRankings.forEach(function (wr) {
                var r;
                if (k == wr.University) {
                    needle = wr.Score_Rank
                }

            })
            div
                .style("left", d3.event.pageX - 100 + "px")
                .style("top", d3.event.pageY - 150 + "px")
                .style("display", "inline-block")
                .html(d.key + "<br>" + "4* Entries: " + d.dataField +
                    "<br>" + "3* Entries: " + s['values'][0]['overall']['3*'] +
                    "<br>" + "2* Entries: " + s['values'][0]['overall']['2*'] +
                    "<br>" + "1* Entries: " + s['values'][0]['overall']['1*'] +
                    "<br>" + "Unclassified Entries: " + s['values'][0]['overall']['unclassified'] +
                    "<br>" + "World Rank: " + needle
                );

        })
        .on("mouseout", function (d) {
            div.style("display", "none");
        })
        .on('click', function (d) {
            var barSel = d3.select(this)
            console.log(barSel.node().classList)
            if (barSel.node().classList.contains("clicked")) {
                barSel.node().classList.remove("clicked");
                removeFromCompare(d.key)
            }
            else {
                //add clicked class for visual
                barSel.node().classList.add("clicked")
                addToCompare(d.key)
            }

        })
        .on("mouseover", function (d) {
            d3.selectAll(".key--" + d.key.replace(/[ ,'.]/g, ''))
                .classed('highlight', true)
        })
        .on("mouseout", function (d) {
            d3.selectAll(".key--" + d.key.replace(/[ ,'.]/g, ''))
                .classed('highlight', false)
        });

    barChart.select(".yaxis")
        .call(yAxis)

    barChart.select(".xaxis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "-.5em")
        .attr("transform", function (d) {
            return "rotate(-90)";
        })
        // as valuses are shortened tool tip shows full name
        .on("mousemove", function (d) {
            var uni = findFromShortStr(uoaAll, d)
            div
                .style("left", d3.event.pageX - 100 + "px")
                .style("top", d3.event.pageY - 60 + "px")
                .style("display", "inline-block")
                .html(uni['values'][0]['Institution name'])
        })
        .on("mouseout", function (d) {
            div.style("display", "none");
        });

}

/**
 * A function that sorts the data in the barchart in descending order
 */
function sortDesc() {
    barData = barData.sort(function (a, b) {
        return d3.descending(a.dataField, b.dataField)
    })
    renderUoaBar(barData)
}

/**
 * A function that sorts the data in the barchart in ascending order
 */
function sortAsc() {
    barData = barData.sort(function (a, b) {
        return d3.ascending(a.dataField, b.dataField)
    })
    renderUoaBar(barData)
}

/**
 * 
 * A function that finds the object corresponding to the selected key. In this case
 * a key is the university name and the object is the one relating to the key
 * containing all associated data 
 * 
 * This function is mainly used for rendering tool tips when hovering a data point
 * 
 */
function findFromKey(uoaAll, key) {
    var needle = uoaAll.find(function (u) {
        return u['values'][0]['Institution name'] == key;
    })
    return needle
}

/**
 * 
 * A function that finds a universities object from a short version of the universities name and returns the full object.
 * This is mainly used when providing tool tip data for the hover over x axes labels
 * 
 */
function findFromShortStr(uoaAll, key) {
    var needle = uoaAll.find(function (u) {
        return u['values'][0]['Institution name'].match(/\b(\w{4})/g).join("") == key;
    })
    return needle
}

/**
 * Function for calculating the uni with the hightst 4* scores for summary and highlight
 */
function calculateTopUni() {
    var topUniScore;

    var closestUniScore;
    var closestUniName;

    topUniScore = d3.max(barData, function (d) {
        return d.dataField;
    })

    topUni = [];
    barData.find(function (d) {

        if (d.dataField == topUniScore)
            topUni.push(d)
    })

    return topUniScore
}